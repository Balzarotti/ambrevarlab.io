/*
Compile with the pthread library. For instance:

  ${CC} -pthread rt.c -o rt

This sample program shows various real-time capabilities of the kernel. We show
that a signal gets instantly caught if the handler has highest priority. Two
tasks are running, main() sends a signal, myfunc() is waiting for it. We measure
the elapsed time between the transmission and the reception of the signal.

On multi-core machines, it is important to set the affinity to
the same CPU, otherwise nothing can be done. Hence the CPU_SET() call.

Possible results:

- PRIO_MAIN > PRIO_THREAD: main() sends the signal, but main() continues until
it has to wait.

- PRIO_MAIN < PRIO_THREAD: main() sends the signal, then the scheduler
instantly switches to the thread.

- PRIO_MAIN == PRIO_THREAD: the results depend on the scheduling policy. Here
we chose the Round Robin policy, so the scheduler will switch alternatively
from one task to the other. This behaviour can be observed through the
number of operations after signal transmission ('ops' variable) if OPSLIMIT
is high enough.
*/

/* Choose on which CPU the tasks should run. */
#define CPUNO 1

/* Define priority for the signal transmitter (main), and for the signal
receiver (the thread). */
#define PRIO_MAIN 95
#define PRIO_THREAD 95

/* Limit the number of operations to count, so that it stops if main() has a
higher priority than the thread. Raise / lower this value to match your
processor speed. */
#define OPSLIMIT 99999999L

#define _XOPEN_SOURCE
#define _GNU_SOURCE
#include <signal.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/time.h>

static struct timeval sigbefore, sigafter;
static volatile sig_atomic_t flag;

void hand() {
	flag = 0;
	gettimeofday(&sigafter, NULL);
	puts("==> Signal caught.");
}

void myfunc() {
	/* CPU affinity. */
	cpu_set_t cpuset;
	CPU_ZERO(&cpuset);
	CPU_SET(CPUNO, &cpuset);
	if (sched_setaffinity(0, sizeof (cpu_set_t), &cpuset) != 0) {
		perror("Thread affinity");
		return;
	}
	fprintf(stderr, "Thread on CPU %d.\n", sched_getcpu());

	/* Task priority. */
	struct sched_param par;
	par.sched_priority = PRIO_THREAD;
	if (pthread_setschedparam(pthread_self(), SCHED_RR, &par) != 0) {
		perror("Thread priority");
		return;
	}

	struct sigaction act;
	act.sa_handler = hand;
	act.sa_flags = 0;
	if (sigaction(SIGUSR1, &act, NULL)) {
		perror("Failed to set signal handler");
		return;
	}

	puts(":: Waiting for signal...");
	gettimeofday(&sigbefore, NULL);
	pause();
	puts(":: End of function.");
}

int main() {
	pthread_t tid;
	struct timeval before, after;

	/* This variable is used to measure the number of operations done by main()
	after the signal has been sent. */
	volatile sig_atomic_t ops;

	/* Flag to measure signal reception. */
	flag = 1;

	/* CPU affinity. */
	cpu_set_t cpuset;
	CPU_ZERO(&cpuset);
	CPU_SET(CPUNO, &cpuset);
	if (sched_setaffinity(0, sizeof (cpu_set_t), &cpuset) != 0) {
		perror("main() affinity");
		return 1;
	}
	fprintf(stderr, "main() on CPU %d.\n", sched_getcpu());

	/* Task priority */
	struct sched_param par;
	par.sched_priority = PRIO_MAIN;
	if (sched_setscheduler(getpid(), SCHED_RR, &par)) {
		perror("main() priority");
		return 2;
	}

	pthread_create(&tid, NULL, (void * (*)(void *))myfunc, NULL);

	/* Let's sleep to make sure thread is correctly initialized. This is bad code,
	but only here for testing. We should use thread barrier instead. Besides, usleep
	is deprecated. The reason for using this is simplicity (and laziness). */
	usleep(500000);

	/* The use of gettimeofday() instead of clock_gettime() is arguable. */
	gettimeofday(&before, NULL);
	if (pthread_kill(tid, SIGUSR1) != 0) {
		perror("Thread had not the time to catch the signal!");
		return 3;
	}
	for (ops = 0; flag && ops < OPSLIMIT; ops++) {
		;
	}
	gettimeofday(&after, NULL);

	pthread_join(tid, NULL);

	fprintf(stderr, ":: Sig TX start: %ld.%ld\n", before.tv_sec, before.tv_usec);
	fprintf(stderr, ":: Sig TX end: %ld.%ld\n", after.tv_sec, after.tv_usec);
	fprintf(stderr, ":: Sig RX start: %ld.%ld\n", sigbefore.tv_sec, sigbefore.tv_usec);
	fprintf(stderr, ":: Sig RX end: %ld.%ld\n", sigafter.tv_sec, sigafter.tv_usec);

	fprintf(stderr, ":: Number of operations after sig TX: %d.\n", ops);

	return 0;
}
