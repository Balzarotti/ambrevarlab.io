#+TITLE: Power Apps
#+DATE: <2015-10-12 Mon>

# TODO: Add homepage links for all apps.

The following programs mostly target power-users and programmers.

Recurring characteristics:

- free software
- lightweight
- fast
- featureful (I want the program to empower me, not to restrict me)
- versionable configuration (plain text, non-XML)
- keyboard-driven
- text-oriented interface

Or more simply put, an Emacs package…

I try to [[../emacs-eshell/index.org][avoid /curses/-based programs]].

Other application listings:

- https://wiki.archlinux.org/index.php/List_of_applications

- https://www.gnu.org/manual/blurbs.html: The GNU software collection contains
  some not-so-famous gems.  I think it deserves more fame.

My personal listing:

+ *3D graphics:* Blender

+ *Archivers:* atool, bzip2, cabextract, cdrtools, cpio, gzip, lha, lrzip, lz4,
  lzop, p7zip, unace, unarj, unrar, unshield, unzip, upx, xz

  - =atool= is a wrapper around many tools to unify compression and extraction
   from command-line.

  - =cdrtools= can process ISO files.

  - =xz= offers a good compression ratio together with great decompression
    performance.  Compression is quite demanding.  =bzip2= has little to offer
    compared to =xz=.  =p7zip= has the features of =tar=, =xz= and =ccrypt= in
    one program.

  - =lrzip= is at its best on large files and with multiple CPUs available.

  - =upx= can compress executables.

  - The rest is for decompression.

+ *Assembly:* NASM, Yasm

+ *Audio editors:* Audacity

+ *Binary file tools:* chrpath, DHEX, Emacs (nhexl-mode), ltrace, nm, od, strace

+ *Bindings:* xbindkeys, xcape, xdotool

+ *Calculation:* bc, calc, Emacs (calc, Lisp), Maxima, PARI/GP, Octave

  - =bc= and =calc= are simple arbitrary-precision calculators.  =bc= is lighter
    but =calc= has somewhat more features.  Both can be used in limited
    environments or in shell scripts.  Emacs' =M-x calc= is more complete and
    more convenient for interactive use.  Emacs also has =M-x ielm=.

  - =PARI/GP= is an extremely fast and advanced algebra system for number
    theory.  Great for prime numbers and such.

  - =Octave= will serve as a Unix-designed Matlab.

+ *Camera capture:* Guvcview, q4vl2

+ *Clipboard tools:* xsel

  Using [[../emacs-everywhere/index.org][Emacs everywhere]], =xsel= is barely useful.  I only use it to yank the
  filename of pictures from =sxiv=.

+ *Contacts:* Emacs (org-contacts)

  I used to use =Abook=, a stand-alone /curses/ contact manager.  Contacts are
  stored in plain text and as such they are versionable, but the automatic
  numbering makes up for huge diffs.  The /curses/ interface is not very
  powerful (limited search capabilities, limited edition).

  =Org-contacts= is much more powerful than =Abook=: free form, arbitrary
  fields, contacts can be fuzzy-searched or retrieved from anywhere within
  Emacs.

  Versioning your contacts is a great way to centralize them, instead of
  spreading them across your mail agent contacts, CSV files, etc.  Plain text
  contacts also means it is easy to write a converter from a CSV file
  (e.g. using Emacs Lisp or =AWK=).

+ *Cryptocurrencies:* electrum

+ *Cryptography:* ccrypt, encfs, GnuPG, pass, Pwgen

  - =GnuPG= is essential at many levels, from communication to data encryption.
    Asymmetric encryption ensures that sensitive data stored on an untrusted
    platform (i.e. online) cannot be compromised remotely by reversing a bad
    password.

  - =ccrypt= is good at encrypting single files symmetrically, e.g. for users
    without a PGP key.

  - =encfs= can encrypt folders and mount them as an encrypted file
    system.  Files can be browsed transparently without being ever written in
    clear to the disk.  It uses a /fuse/ backend which makes it portable across
    systems at the expense of speed.

	- [[https://www.passwordstore.org/][=pass=]] is one of the few password managers that can be used to safely store
    and synchronize passwords online.  It uses asymmetric encryption (through
    =GnuPG=).  Git is used to synchronize the password store.  I recommend using
    Emacs' =helm-pass= to fuzzy-search the passwords on input.

+ *Dictionaries:* aspell

+ *Diff tools:* Emacs (ediff, ztree), diffoscope

+ *Disk utilities:* gparted, ncdu, parted, rmlint, shred, trash-cli, tree, wipe

  - =ncdu= is a very fast and convenient disk usage analyzer.  Too bad it's in
    /curses/ though, I'm still looking for a good Emacs alternative.

  - =trash-cli= is a command-line interface implementing FreeDesktop.org's Trash
    specification.  It can be combined with a file browser for easier use.  Emacs
    also has the =delete-by-moving-to-trash= variable.

  - =wipe= can delete folders securely while =shred= can only process files.

+ *Document processing:* Emacs (Org-mode), lilypond, pdf2svg, Texinfo, TeX Live

  Consider using Org-mode over other markup formats like Markdown.  See
  http://karl-voit.at/2017/09/23/orgmode-as-markup-only.

  Prefer Texinfo over Man pages which are very limited.  Most /man/ readers
  don't support cross-references, fuzzy search or section browsing.  Both Emacs'
  =M-x man= and =M-x woman= support those features however.  Texinfo manuals
  automatically generate an index which is extremelly useful for browsing.

  TeX-based processors offer the best typographic rendering while based on what
  is possibly the worst programming language.  Org-mode has good export support for
  TeX / LaTeX.

+ *Document readers and tools:* antiword, apvlv, catdvi, catdoc, docx2txt,
  Emacs (pdf-tools), evince, ghostscript, odt2txt, mcomix, poppler, pstotext,
  unrtf, wv, xchm, zathura

  - =apvlv=, =pdf-tools= and =zathura= are light and keyboard-driven.  =zathura=
    supports PostScript =.ps= files and SyncTeX.  =apvlv= supports UMD, HTML and =.txt=
    files.  =pdf-tools= supports SyncTeX and has good text search and selection
    facilities, it enables many Emacs features (Helm, Ivy).

  - =Evince= is far too heavy but can fill PDF forms.  (Yet another ill-conceived
    PDF feature…)

  - =Poppler= has numerous PDF converters (e.g. /pdftotext/).

  - =ghostscript= can convert PDF to and from PS.

  Most of these programs can be used for previewing document formats in text
  form.

+ *E-mail clients:* Emacs (mu4e, notmuch)

  I used to use =Mutt=: it's extremely hard to configure for a very limited
  result.  It does not support viewing e-mails and composing at the same time;
  you can't really copy text from the pager; multiple-account support is rather
  tedious to configure.

  =mu4e= is moderately easy to configure, extremely extensible with Emacs Lisp
  and does not suffer from the limitations of Mutt.  You can fuzzy-search
  contacts, e-mails, preview HTML, display embedded pictures and much more.

  =notmuch= has supports tagging and tree display of threads:
  https://notmuchmail.org/screenshots/#index4h3

+ *File browsers:* Emacs (dired, Helm Find-Files), ranger

  - I used to use =ranger=, but being /curses/-based, it can only be that
    useful.  The column display is nice though.  It can preview all sorts of
    file but graphics display is obviously limited.  Emacs-based browsers are
    much better at this.  It can run an arbitrary command on any file selection
    and it remembers the selection in every folder.  It can run various powerful
    commands conveniently, such as recursive hardlink creation or
    batch-renaming.

  - Emacs' =Helm Find-Files= makes for a revolutionary file browser: typing
    anything will fuzzy-filter the current directory, or even subdirectories.
    The filtered results can be browsed with special keys: you can select files
    from different directories and apply arbitrary actions to them.  It has a
    binding to switch to =Eshell= from the currently-browsed folder.  It's
    possible to batch-rename selected files from multiple folders using
    =Wdired=.

+ *File synchronization:* hsync, rsync

  - Read the documentation carefully.  =rsync= has a lot of useful options, like
    =--append-verify=, =--progress= and =--size-only=.

  - [[../hsync/index.org][=hsync=]] is useful to detect renamings and can be called before =rsync= to
    speed up the process.

+ *Finders:* Emacs (Helm, Ivy)

  I used to really like =fzf= and search programs like =agrep= (from the =tre=
  package) or =ag= (The Silver Searcher).  If you live in Emacs, those are
  completely superseded by Helm or Ivy.

  Emacs interactive completion can effectively be used everywhere: for
  structured documents, code completion, file search, command history, folder
  history and more.

+ *FTP clients:* curlftpfs, Emacs (TRAMP), NcFTP

  Emacs' TRAMP allows you to work on remote files (move, delete, download) and
  edit them transparently: first they are automatically downloaded, then all
  edits are done locally within Emacs, and last the file is uploaded upon
  saving.

+ *FTP servers:* vsftpd

+ *Gaming emulators:* DGen, DOSBox, Gens/GS, Lutris, Mupen64Plus, PCSX-Reloaded,
  MAME, Wine

  Lutris attempts to provide a one-click install & play interface.  It's
  especially useful for games that are tricky to configure right.

+ *Gaming tools:* DriConf, TiMidity++, xosd

  - =osd_cat= from =xosd= can display text on screen, such as FPS or network
    traffic.

  - =TiMidity++= is useful for old games using midi music.

+ *Job scheduling:* mcron

  - =mcron= is a rewrite of the venerable =cron= in Scheme (Guile).  Jobs can be
    declared in Scheme, which allows for programming jobs.

+ *Local file sharing:* woof, python

  FTP is sometimes blocked by routeurs, plus HTTP is more accessible for people
  running poor systems.

  - =woof= is very convenient to share single files over HTTP with anyone.  It
  also support an upload form so that other people can send you files.  =woof=
  does not support sharing of directories (without taring them first, that is to
  say).

  - =python= also comes with a simple HTTP server:
  #+BEGIN_SRC sh
  python3 -m http.server
  #+END_SRC
  or, with Guix:
  #+BEGIN_SRC sh
  guix environment -C -N --ad-hoc python -- python3 -m http.server
  #+END_SRC
  The python HTTP server automatically shares the entire current directory.

+ *Music:* Beets, clyrics, cmus, Demlo, Emacs (EMMS), mps-youtube, youtube-viewer

  - =clyrics= can display the lyrics of the song currently playing in =cmus=.

  - The =cmus= music player is extremely fast at updating its database.  The
    /curses/-based UI is a bit limitating though.

  - Emacs' EMMS is similar to cmus with the bonus that it is extensible, it can
    fetch lyrics, it can resume state upon restart and it can display album covers
    within the music library tree.

  - =mps-youtube= can build albums from Youtube links automatically and save the
    resulting playlist locally.  Since it's terminal-based it cannot display
    thumbnails.

  - =youtube-viewer= is similar to =mps-youtube= but its graphical interface can
    display thumbnails which is very convenient to help the user find a video.
    I find it less convenient than =mps-youtube= to build playlists though.

+ *Network monitors:* Aircrack-ng, ngrep, nmap, speedtest-cli, Tcpdump, Wireshark

+ *News readers:* Emacs (elfeed)

  - Elfeed is better at rendering HTML feeds than =newsbeuter=, it supports
    image display, it can fuzzy-search everything.

+ *Package management:* Emacs (helm-system-packages), Guix, Stow

  - =Guix= can be installed on any system.  It allows for rollbacks, unprivileged
    package management, etc.  It's extensible in Scheme (Guile).

  - =Helm System Packages= is a universal interface to your package manager.  It
    lets you fuzzy-search and execute complex actions over arbitrary selections
    of packages.

  - GNU =Stow= is convenient to install packages locally (without using a
    package manager).  I also find it convenient for dotfiles management.

+ *Picture batch processors:* dcraw, Emacs (gif-screencast), gifsicle,
  GraphicsMagick, ImageMagick, jpegtran (from libjpeg-turbo), optipng

  - =dcraw= can convert many camera raw formats.

  - =GraphicsMagick= and =ImageMagick= are very similar in features and they may
    be complementary regarding performance.

+ *Picture editors (Raster graphics):* darktable, GIMP, Hugin, Metapixel, RawTherapee

+ *Picture information:* perl-image-exiftool

+ *Picture viewers:* feh, sxiv

  - =feh= can set the wallpaper.  =sxiv= is very fast at loading and displaying big
    pictures.  It supports GIF animations unlike =feh=.

+ *Power usage monitors:* powertop

+ *Programming:* cloc

+ *Programming in C:* cppcheck, GDB, mcpp, musl, Splint, TCC, Valgrind,
  Uncrustify

  - =cppcheck= and =Splint= are static analyzers with overlapping features.

  - =Uncrustify= is much better engineered than =GNU Indent= and =Astyle=.  See
    the [[../indentation/index.org][Indentation rationale]].

+ *Screen capture:* scrot

+ *Screen control:* light, xbacklight

  - =light= works without X as opposed to =xbacklight=.

+ *Session locks:* slock, vlock, xlockmore, xss-lock

  - =slock= is as simple as it gets but does not support PAM sessions unlike
    =xlockmore=.

  - =vlock= is for TTY sessions.  It is part of the =kbd= project.

  - =xss-lock= auto-locks the screen on standby or when the laptop lid is closed.

+ *Shell:* DASH, dtach, Emacs (Eshell), fish, shellcheck

  - =DASH= is a light, fast and POSIX compliant shell.  It is quite limited for
    interactive use but ideal for testing the POSIX-ness of scripts.

  - =dtach= detaches the command passed as argument, allowing it to run in the
    background even after the parent program has been killed.  The backgrounded
    program can be re-attached at any moment to another shell.  =dtach= also
    works with Eshell.

  - =fish= departs from the POSIX-derived shells.  =Bash= suffers from the
    design issues of the venerable Bourne shell (e.g. word-splitting).  =Zsh=
    has tried to unite all shell languages under one banner, thus becoming
    complicated beyond reason to the point that the simplest configuration can
    be an Odyssey on its own.  Like =rc=, =fish= uses a clear syntax.  It also has
    a straightforward API, which makes it very straighforward to customize and
    extend.  Last but not least, its interactive features are efficient and to
    the point.

    The lack of POSIX-ness is no problem in practice:

    - Any POSIX shell script will be executed by the interpreter pointed by the
      shebang.

    - Initialization files such as =.profile= can still be set up by =sh= at the
      beginning of the session: use =sh= as your login shell and =exec fish= at
      the end.

  - Farther down the road of non-POSIX shells, Eshell lies even further: it uses
    Emacs Lisp as a language which is arguably much more powerful than =fish=.
    See [[../emacs-eshell/index.org][my article on Eshell]] for more good reasons why you should use it.

    Eshell has a very powerful completion framework, =pcomplete=.  As of 2017
    the limited popularity of Eshell result in limited support for completion.
    That being said, it is possible to configure Eshell so that [[https://gitlab.com/ambrevar/emacs-fish-completion][it falls back on
    the completion of =fish=]], which then makes for a very extensive completion
    support.

  - =shellcheck= is a static-analyzer for shell scripts.

+ *Spreadsheet:* Emacs (Org-mode)

  - This =Emacs= mode lets you write plain text tables (track them with =git=!)
    and apply arbitrary functions to cells.  These functions are either
    pre-defined or self-written in Lisp (with ubiquitous support for =M-x calc=
    and its arbitrary precision arithmetic).  From there you can use every Elisp
    feature, and if that would not be enough (e.g. too slow) you can call
    external programs to perform the task, say, your favourite scripting
    language.  This makes the tables infinitely programmable.

+ *System information:* inxi

+ *System monitors:* Emacs (proced, helm-top), htop, iftop, Iotop, lsof,
  NetHogs, PSmisc

+ *Task management:* Emacs (Org-mode)

  - Org-mode all the way.  It can be used as TODO manager, calendar, for
    document processing, documentation, literate programming, etc.

    I briefly tried =Taskwarrior=, which file format is plain text but hard to
    read.  The editing is far less convenient than with a proper text editor.
    This is where the power of using an editor as a user interface really
    shines.  Org-mode is not an Emacs exclusivity, some other editors support
    it.

+ *Text editors:* Emacs

  [[https://www.youtube.com/watch?v=JWD1Fpdd4Pc][Yes, you should use Emacs]].

+ *Text processing:* Emacs Lisp, recutils

  Emacs Lisp makes it very convenient to batch-edit text files using its
  "buffer-editing" model (i.e. scripts can directly reflect interactive
  editing).  I find it often much more efficient to write than AWK scripts,
  especially with its step-by-step debugging capability.

+ *Touch-typing:* GNU Typist

+ *Torrent clients:* Transmission

  - =Transmission= is full-featured and offers various UIs: GTK, Qt, curses,
    and… Emacs! (=transmission.el=)  Beside not supporting magnet links,
    =rtorrent= has a poor UI for selecting files and folders manually, which
    makes it very impractical for large torrents.  The Emacs interface brings in
    its load of usual advantages: extensibility, keyboard-driven, fuzzy-search,
    macros, etc.

+ *Transcoding:* cdparanoia, dvdbackup, FFmpeg, flac, Gaupol, id3v2, libvpx,
  MediaInfo, mkvtoolnix, opus, vorbis-tools, wavpack, x264

  - =FFmpeg= is the swiss-army knife of transcoding: aspect ratio, concat, crop,
    mixdown, remux, metadata, etc.  It is much more efficient to use FFmpeg from a
    smart custom script than using a GUI.

  - =mkvtoolnix= can process mkv files in place, e.g. it can instantly change
    metadata.

  - =cdparanoia= rips audio CDs.

  - =dvdbackup= decrypts VOB files.

  - =Gaupol= is a simple yet complete subtitle editor.

  - =MediaInfo= displays the media property of pictures, audio and video files
    (codecs, container, etc.).  It overlaps a lot with =FFprobe= (from =FFmpeg=),
    but still manages to provide some details that =FFprobe= misses.

  - The rest is a set of tools for containers and codecs.

+ *Translation:* translate-shell, Emacs (google-translate)

+ *Unit conversion:* units

+ *Vector graphics:* Asymptote, Graphviz, Inkscape

  - =Asymptote= is a full-featured descriptive vector graphics renderer.  It
    features libraries for: plots, trees, 3D (with perspective!), and much more.
    The language is much more convenient (C-style) and far less limited than its
    competitors (TikZ, Metapost, PSTricks): it has double-precision arithmetic
    support, control structures, data structures, support for custom structures,
    etc.  It also supersedes Gnuplot.

  - =Graphviz= is a smart graph drawing tool that will decide automatically of the
    best arrangement for the vertices and edges.

  - =Inkscape= can export to LaTeX, which is useful for good and consistent
    typography in your document.

+ *Version control:* git, Emacs (Magit)

  I use the =netrc= credential system (from =git contrib=) to re-use the
  credentials stored in my =~/.authinfo.gpg= when calling =git send-email=.

+ *Video:* mpv, subdl

  - =mpv= is a fork of =mplayer= with fewer dependencies and some additions such
    as an on-screen display, support for resuming and chapter markers.  Both =mpv=
    and =mplayer= allow for very fast video rendering, which can render 1080p
    videos on lower-end machines where =VLC= would stutter.

  - =subdl= will often fetch the right subtitles for the desired language.  When it
    fails to pick the right one, it is still possible to select it manually.

+ *Virtual machine hypervisors:* QEMU

+ *Web browsers:* Emacs (eww), Next, qutebrowser, surfraw, tor-browser, w3m

  - While =eww= is text-based, it can render variable width/height fonts as well
    as pictures.

+ *Web tools:* curl, Wget, youtube-dl

  - =curl= and =Wget= are overlapping but also very complementary.

  - =youtube-dl=, as the name does not imply, is not restricted to YouTube.

+ *Window managers:* Emacs (EXWM), Stumpwm

  I've used =Awesome= (extensible in Lua) and =i3= (relies on external scripts
  for extensibility).  In the end their extensibility was always too limited for
  my needs.  For instance it's very hard (impossible?) to search a window by its
  title and directly focus on it.

  - =EXWM= is a WM where all windows are Emacs buffers.  Consequence: you can
    fuzzy-select your windows, tile your selection, delete the complementary
    selection, etc.  EXWM is obviously extensible in Emacs Lisp.

  - =Stumpwm= is 100% extensible in Common Lisp.  It's possibly the most
    featureful window manager.  It's less integrated to Emacs, which has its ups
    and downs: a blocking Emacs won't block everything, but it's harder to link
    Emacs content with external applications.
