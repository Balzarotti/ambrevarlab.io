size(300);
import bsp;

// currentprojection=perspective(-10,2,-2, up=Y);
currentprojection=perspective(-4,4,-5, up=Y);

path3 screen=shift(-X/2-Y/2)*plane(X, Y);
screen=shift(Z)*scale3(6)*screen;

draw("Projective plane", project(screen),S, blue);

draw(Label("$X$", EndPoint), project(O--X), gray, Arrow);
draw(Label("$Y$", EndPoint), project(O--Y), gray, Arrow);
draw(Label("$Z$", EndPoint), project(O--Z), N, gray, Arrow);

dot("$(0,0,1)$", project(Z), 2*E);

path3 axisback=(4*Z--(Z));
path3 axis=((Z)--(-3*Z));
draw(project(axis));
draw(project(axisback), dashed);

// Points
triple p = (-1, 2, 1);
triple pp = (-2, 1, 1);

dot("$P$", project(p));
dot("$P'$", project(pp));
draw("3D shearing", project(p--pp), 2E, Arrow, Margin(0.8, 0.8));
