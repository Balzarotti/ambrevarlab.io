size(300);
import bsp;

// currentprojection=perspective(-10,2,-2, up=Y);
currentprojection=perspective(-5,4,-5, up=Y);

// Screen
path3 screen=shift(-X/2-Y/2)*plane(X, Y);
triple screen_centre = Z;
screen=shift(screen_centre)*scale3(6)*screen;
draw("Screen", project(screen),S, blue);

// Base
draw(Label("$X$", EndPoint), project(O--X), gray, Arrow);
draw(Label("$Y$", EndPoint), project(O--Y), gray, Arrow);
draw(Label("$Z$", EndPoint), project(O--Z), N, gray, Arrow);

// Camera
triple camera = (0, 0, -1);
dot("Camera $(0, 0, z_c)$", project((0, 0, -1)), SE);

// Points
triple p = (0, 2, 2);
dot("$P = (i, j, k)$", project(p));

triple pp = intersectionpoints(p -- camera, surface(screen))[0];
dot("$P' = (i', j', k')$", project(pp), W);

triple base = (p.x, 0, p.z);

dot("$(0, 0, z_s)$", project(screen_centre), 2*E);

// Triangle
draw(project(pp -- camera -- screen_centre));
draw(project(screen_centre -- base -- p -- pp), dashed);

