(find-file "index.org")
(org-md-export-to-markdown)
(find-file "index.md")
(goto-char (point-min))
(insert "title: A packaging tutorial for Guix
date: 2018-10-09
author: Pierre Neidhardt
tags: Software development, Programming interfaces, Scheme API
---
")

(goto-char (point-min))

;; (save-excursion
;;   (while (search-forward "</sub><sub>" nil 'noerror)
;;     (replace-match "_")))
(save-excursion
  (while (search-forward "<sub>" nil 'noerror)
    (replace-match "_")))
(save-excursion
  (while (search-forward "</sub>" nil 'noerror)
    (replace-match "")))

(while (re-search-forward "\n\n    +" nil 'noerror)
  (let ((language-code (pcase (char-after)
                         (?$ "sh")
                         (?> "scheme")
                         (_ "scheme"))))
    (forward-line -1)
    (newline)
    (insert (format "```%s" language-code)))

  (re-search-forward "\n\n+[^ ]")
  (goto-char (match-beginning 0))
  (newline)
  (insert "```"))

(save-buffer)
