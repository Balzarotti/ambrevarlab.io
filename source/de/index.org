#+TITLE: Building your own desktop environment
#+DATE: <2016-09-28 Wed>

All-in-one desktop environments (DE) such as Gnome, KDE and those found in
popular systems get in the way of the power user in that they do not leave much
room for customization, it restricts the potential a computer can provide.
Moreover most popular DE have been plagued for decades with unpractical,
non-automated and limited window management.

Positioning and resizing windows does not take much time /per se/, but it stacks
up.  Think about how many windows you fire up every day (which, for some obscure
reasons, seem to be spawned at random places all the time on many popular
systems). Add up all the seconds you spend managing them, moving around,
resizing…  Wouldn't it be nice if this were done for us?  After all, a computer
is an augmented /automata/, it should be able to automate this.

Another side-effect: the classic desktop environment (DE) will bundle more than
you want…  As such, they will always require more disk space and virtual memory
than you actually need.  The following advises will make things lighter and
faster.

* Dynamic Tiling Window Managers

There are ways to automate window management: tiling window managers.  Once we
have gotten used to them, it gets hard to bear with the waste of time incurred
by the "regular, user-friendly" window managers.  (Like those featured in Gnome
and KDE.) The latter are called stacking window managers.  I find the concept of
stacking fairly questionable: why would the user want to /cover/ part of other
windows? Either you want to have a full look at some of the currently opened
windows, or you just want to focus on one windows.  Which means either tile the
windows or go fullscreen.  In other words, unused screen space is wasted screen
space.  I know how badly we like to look at our wallpaper, but that is not the
primary use we should make of our computer.

This leads us to the antagonists of the stacking window managers: the tiling
window managers.  Windows do not "float" anymore, i.e. their position and size is
not controlled manually but automatically so that it fits the screen space best.
Understand that tiling managers do no deprive you from manual control over the
windows: they are, by definition, a superset of stacking WMs, and thus you can
still resize and position windows as you see fit.

The core idea is to be fast and display what the user wants.

See how mobile devices work: the restricted screen space does not allow for
stacking windows, so all applications go fullscreen.

TWMs allow the user to relay full window management to the WM independently of
the application capabilities.  For instance a good WM will let you go fullscreen
or use tabs with any program. This is part of the [[http://en.wikipedia.org/wiki/Unix_philosophy][Unix philosophy]]: instead of
duplicating functionalities across programs, write it once and for all where it
is meant to be.  File browsers and Internet browsers should not have tabs: WMs
will do a better job (grouping, sorting) while keeping consistent behaviour and
key bindings across programs.

Last but not least, there is a subcategory among tiling WMs: the dynamic ones.
The tiling is done whenever a window is fired up.

In theory, the concept of a window manager is rather simple.  In practice, they
mainly differ by their configuration.  Some WM will offer little tweaking
possibilities to the user, while other will allow programmable modifications.
The former is easier to get started with, while the latter will allow for the
exact desired behaviour.

In my opinion, the most powerful WM would be a DTWM with a Turing-complete
configuration.  Such popular DTWMs include /Awesome/ and /i3/. Awesome's
configuration is in Lua, i.e. it allows virtually unlimited tweaking, while i3
relies on external shell scripts.  Another possibly more powerful example is
[[../emacs-everywhere/index.html][EXWM]] which is extensible in Emacs Lisp.

* Keyboard control

The use of the mouse has become so widespread with the advent of desktop
computers that it now seems "intuitive" to associate the use of a mouse together
with desktop environments.  All the desktop management (windows, firing up
programs, monitoring the system, etc.) are /discrete actions/, they do not
require the continuous movement of a mouse pointer.  As such, everything can be
bound to a keybinding.  Go fullscreen?  Keybinding.  Change the window layout?
Keybinding.

Keep in mind we say "keyboard shortcut": the word /shortcut/ is there for a
reason.  See my article on [[../keymap/index.html][mastering the keyboard]].

* Building from bricks

Refraining from using a full-featured desktop environment means that you will
have to install the components one by one and configure it all to your liking.
Which is the only way to fully tailor a computer to your needs.  While it may
seem daunting at first, this is a one-time task: it is straightforward to re-use
the configuration across systems when it is stored in text files.

** Text configuration

Most desktop environments come with a configuration panel: a program that is
meant to centralize configuration.  In practice, this is an additional layer of
complexity that is not really needed.

Editing a text file has many advantages:

- It is just as fast.  Even faster with a good text editor.
- A GUI may not be exhaustive: a text file guarantees full access to the
  options.
- It is more reliable since it removes the extra layer of complexity.  (The GUI
  might introduce bugs.)
- You keep control over configuration file updates.
- The configuration can be saved and even put under version control.

This last point is key: it allows you to watch over the evolution of your
configuration changes (diffs); it also lets you deploy and sync a configuration
across several computers in an instant.  Last but not least, it lets you share
the configuration!

** Independent power-apps

Desktop environments push for developing "desktop-specific" applications, such
as the K* programs for KDE vs. the G* programs for Gnome.  I find this very
embarrassing: why would we spend months and years developing a program for a
sub-group of people?  Why not spreading it to the world?  Is it a form of
geek-racism?

Our favorite programs should not have to do with the desktop environment.  If the
latter goes out of fashion and gets unmaintained, does it mean we are supposed
to change the whole set of our programs, possibly losing some functionalities in
the process?

A "desktop" in my sense is just the WM with a launcher.  The rest should be the
user favorite programs: a terminal emulator, a desktop monitor, a text editor,
etc.  Every feature from a desktop environment is a program.  Missing this volume
icon?  Add key bindings.  Want to tweak the network configuration?  Fire-up the
appropriate tool with a keybinding, or go edit the text file full of endless
possibilities…

See my [[../power-apps/index.html][list of power-user applications]] for some
inspiration.

* Black & White

When the mere mortals have a glimpse at my computer, I would expect their first
reaction to be "What system is it?" or "Why is it so barren?".  No, it is
invariably "Maaaah, why is it all black?!?"

I find it interesting that black has become shocking.  Black used to be the
standard back in the days.  The white-dominant background may have been
popularized by the "user-friendliness" marketing of Apple and Microsoft in the
1980s as a reaction against the "hard-to-use" Unix and friends: the color was
here to mark the difference.  The association of ideas was easily made: white is
brightness, it is light, as opposed to the uncomfortable, insecure darkness of
Mr. Black.

Now if we are rational, it is just a color.  But there is more to it.  Working on
a dark background will not strain the eye as much.  The whole industry is warning
its professional, intensive users against the possible eye strains and headaches
resulting from staring at a screen for too many hours.

Solutions are developed: take a break, use eye drops, and whatnot.  While it
would be so much easier to eliminate the problem in the first place…  Listen to
the wisdom of our forefathers, they were bright people!  Working on a dark
background will solve the problems all at once. To the point that once you get
used to it, switching to a bright background will stun you on the spot!
