#+TITLE: Indentation: A Rationale
#+DATE: <2014-11-12 Wed>

This article sketches a rationale on source code indentation.

Indentation is a never-ending holy war between programmers.  The divergence that
resulted led to the development of many complex tools (GNU indent, Uncrustify,
to name a few) and every decent text editor to implement a complex indentation
engine.

Teams have to reach an agreement on indentation.  If you happen to be part of
several teams, chances are high that you have to work with different
conventions.

This pushes editors to implement yet another complex and unreliable feature:
indentation guessing.

Versioning and =diff= tools bring yet more trouble to the case.  A simple
open/save on a file with a different editor than the original can lead to a huge
=diff=.

Discussions on how to solve this problem have proven to inevitably reach a
dead-end.  The following sketch of rationale does not claim to solve it either,
however I would like to point out how simple measures taken right from the
inception of a programming language can help reducing all the hassle the
indentation question creates.

* Rationale

Main arguments:

- Readability matters.  Code is read more than it is written.
- Code changes should be versioning-friendly.
- Freedom of form for clarity where that matters.  Tools should not constrain
  you.

Lesser arguments:

- The less bytes the better.
- The indentation engine should be short and simple.

* The Go example

Let us consider the Go language (first issued in 2010).  The official
implementation ships with an indentation/formatting tool called =gofmt=.  From
the [[http://blog.golang.org/go-fmt-your-code][official website]]:

#+BEGIN_QUOTE
Gofmt'd code is:

- easier to write: never worry about minor formatting concerns while hacking
  away,
- easier to read: when all code looks the same you need not mentally convert
  others' formatting style into something you can understand.
- easier to maintain: mechanical changes to the source don't cause unrelated
  changes to the file's formatting; diffs show only the real changes.
- uncontroversial: never have a debate about spacing or brace position ever
  again!
#+END_QUOTE

This basically brings an end to any of the aforementioned conflicts on
indentation.

=gofmt= is a tool for both indenting and style.  Important things to note
however, it will not

- touch your comments;
- split nor join lines;
- change empty lines.

However, it will:

- change spacing between tokens;
- indent your code conforming to one precise style (more or less in K&R style);
- tabify the code (no spaces, no alignment).

Let us analyze these choices.

- Comment style is extremely varied, and there is no clear winner.  =gofmt= is a
  /standard/ tool, and as such it does not claim to impose a comment style.

- Line splitting depends on line width (which value is left to the programmer's
  choice), and /where it feels right/.  This can obviously not be determined
  computationally.  So it seems wise not to touch it.  However it does merge
  multiple empty lines between two comments.  I am not sure why it is being
  inconsistent here.

- Empty lines are used to separate parts of the program, again, /where it feels
  right/.  This should not be changed either.

- It is very hard for a human being to be consistent with token spacing.  Rules
  can vary here.  All that matters is that the end result is consistent and
  readable.

- Same thing for indentation.

- The use of tabulations over spaces can be argued.  Same thing for alignment,
  despite being more subject to personal opinions.  I will discuss this in later
  sections.

Now let us come back to our rationale.

-
  #+BEGIN_QUOTE
  Readability matters. Code is read more than it is written.
  #+END_QUOTE

  The choice of style for indentation and token spacing is good enough: it is
  close to K&R, a style which has been widely accepted and source of inspiration
  for many other styles.

-
  #+BEGIN_QUOTE
  Code changes should be versioning-friendly.
  #+END_QUOTE

  =gofmt= ensures this.

-
  #+BEGIN_QUOTE
  Freedom of form for clarity where that matters. Tools should not constrain
  you.
  #+END_QUOTE

  =gofmt= leaves you freedom on how you separate parts of your code with
  linebreaks (except for comments) and how you format comments.  What cannot be
  decided computationally is left to the author.

-
  #+BEGIN_QUOTE
  The less bytes the better.
  #+END_QUOTE

  The use of tabs and no alignment help.

-
  #+BEGIN_QUOTE
  The indentation engine should be short and simple.
  #+END_QUOTE

  Rules are just a few.  No alignment helps.

* A solution

=gofmt= choices look reasonable in regard to our rationale.  But the quality of
the style are not worth discussing in face of the invaluable advantages it
provides.  Its ultimate goal, as stated in the above quote, is not to format
code, it is to relieve the programmer from the burden of style and indentation,
leaving focus on content.

Being part of the standard distribution helps a great deal to bring an end to
the style and indentation wars.

The solution to the style and indentation problem is not lying in a helpless
attempt to find the best rules, it lies in providing a /standard/ formatting
tool for a language that follows some rational criteria.

If a language has been out for years without a formatting tool, as it is the
case for C, it is problably already too late.  Current formatting tools for C are
fragmented.  They run in a significantly different manner than Go: instead of
enforcing one common style, they are made configurable to adapt everone's style.
It makes sense since none of these tools are standard, they cannot pretend to
impose a style.

Some other language took a different approach by only publishing a rationale on
style.  While this is a respectable decision, it is not as helpful as providing a
standard formatting tool: programmers still have to set up their editor properly
in accordance to the rationale, and some are simply not willing to make the
effort.

In the next sections, I will argue more on specific indentation choices.

* Tabulations

The use of tabs over spaces is a source of flaming debates.  However it seems
like there is a (not so overwhelming) majority of programmers using /tabs only/
or /tabs + spaces for alignment/.

Pros:

- It uses less bytes as long as indentation is >1.  Who uses an indentation of 1
  anyway?

- The tab width is adaptable live.  This it /the one argument/ for tabs against
  spaces.  Values between 2 and 8 are customary.  2 leaves more space on screen
  which can be handy in restricted conditions.  But 8 allows for more clarity,
  in particular to distinguish big nested blocks.  The [[https://www.kernel.org/doc/Documentation/CodingStyle][Linux kernel style]]
  provides a rationale for using a tab width of 8.

  When using spaces, if one uses 2-spaces indentation rule, the code can not be
  made more readable without actually modifying the file.

Cons:

- The code will not display identically across different configurations.  But in
  regard to the previous point, this is a feature, not an issue.

Back to the rationale:

-
  #+BEGIN_QUOTE
  Readability matters. Code is read more than it is written.
  #+END_QUOTE

  Tabs allow for flexibility in readability.

-
  #+BEGIN_QUOTE
  Code changes should be versioning-friendly.
  #+END_QUOTE

  Changing the tab width does not change the file. Changing space indentation
  does.

-
  #+BEGIN_QUOTE
  Freedom of form for clarity where that matters. Tools should not constrain
  you.
  #+END_QUOTE

  Tab width is left to the user taste and needs.

-
  #+BEGIN_QUOTE
  The less bytes the better.
  #+END_QUOTE

  Tab use less bytes than spaces.

-
  #+BEGIN_QUOTE
  The indentation engine should be short and simple.
  #+END_QUOTE

  N/A.

* Alignment

Alignment is mostly seen in tables and in function calls:

#+BEGIN_EXAMPLE
foo = { bar,
        baz }

foo ( bar,
      baz )
#+END_EXAMPLE

Pro:

- It makes structured code look clear.

Cons:

- Alignment, as for style, is determined by many parameters. The question of
  what part of the code should be aligned is hard to answer both for humans and
  machines.

  Body only:

  #+BEGIN_EXAMPLE
  int foo[3] = { bar,
                 baz,
                 qux
  };
  #+END_EXAMPLE

  Body + openers/closers:

  #+BEGIN_EXAMPLE
  int foo[3] = { bar,
                 baz,
                 qux
               };
  #+END_EXAMPLE

- Alignment is hard to implement in formatters and editors. All the parameters
  must be configurable, or else the tool is constraining the formatting.

  For example, if alignment yields an ugly result as in

  #+BEGIN_EXAMPLE
  foobar('arg1', function ()
    print('foobar')
  end,
         'arg3'
  )
  #+END_EXAMPLE

  one can "fix" this by changing the outline:

  #+BEGIN_EXAMPLE
  foobar('arg1',
         function ()
           print('foobar')
         end,
         'arg3'
  )
  #+END_EXAMPLE

  In that case you cannot control clarity, you are forced to obey to the
  limitation of your editor.

Alignment can bring some clarity. As such, our rationale sounds very favourable:

#+BEGIN_QUOTE
Readability matters. Code is read more than it is written.
#+END_QUOTE

However it should be noted that unaligned but indented code is notwithstanding
readable:

#+BEGIN_EXAMPLE
foo = {
    bar,
    baz
}
#+END_EXAMPLE

And the other parts of the rationale are not as positive:

#+BEGIN_QUOTE
Code changes should be versioning-friendly.
#+END_QUOTE

This is never the case with alignment:

#+BEGIN_EXAMPLE
foo = { bar,
        baz
}


foobar = { bar,
           baz
}

foo = {
    bar,
    baz
}
#+END_EXAMPLE

#+BEGIN_QUOTE
Freedom of form for clarity where that matters. Tools should not constrain
you.
#+END_QUOTE

This is not the case if your editor is not customizable enough to let you have
fine-grained control over the alignment rules.

#+BEGIN_QUOTE
The less bytes the better.
#+END_QUOTE

Alignment is always using more bytes than unaligned code.

#+BEGIN_QUOTE
The indentation engine should be short and simple.
#+END_QUOTE

Alignment is obviously harder to implement than no alignment at all; moreover,
it can be quite tricky to implement a generic alignment engine that is
customizable enough so as not to impede freedom of form.

In practice, I believe that the fanciness alignment provides is not worth its
downsides.

** Other considerations

*** Inner alignment

#+BEGIN_EXAMPLE
map {
  key       = value1,
  longerkey = value2,
}
#+END_EXAMPLE

Inner alignment is independent of indentation, and should use spaces only.  It
often increases clarity in long and complex structure.  The downside is that as
soon as you add an entry that does not fit the alignment, you will have to
re-align everything.  This is a bit tricky to do automatically with the most
advanced editors, while being impossible with all the others.

*** End-of-line alignment

This is usually restricted to comments.  One should never spread end-of-line
comments over different indentation levels.  In the following example with a tab
width of 2,

#+BEGIN_EXAMPLE
if (foo) { // Long comment
  bar;     // spread over
}          // Multiple line
#+END_EXAMPLE

switching to a tab width of 4 will break the alignment.

#+BEGIN_EXAMPLE
if (foo) { // Long comment
    bar;     // spread over
}          // Multiple line
#+END_EXAMPLE

* Tools

The only help you can find for incurable languages like C lies in formatting
tools. They will help a team working with a consistently formatted code base.

** GNU indent

It uses the GNU style by default, and every option is meant to change its
behaviour starting from there.  This is hardly transparent or convenient.

GNU indent comes with the possibility to split or join lines.  If this option is
set, and maximum line-width is set to, let us say 70 characters, it will
transform

#+BEGIN_EXAMPLE
if (long_condition1 && long_condition2 && long_condition3 && long_condition4) {
#+END_EXAMPLE

to

#+BEGIN_EXAMPLE
if (long_condition1
    && long_condition2
    && long_condition3
    && long_condition4) {
#+END_EXAMPLE

If line splitting is off, then it will force joining lines!  I.e. it will
transform

#+BEGIN_EXAMPLE
if (long_condition1
  && long_condition2
  && long_condition3
  && long_condition4) {
#+END_EXAMPLE

to

#+BEGIN_EXAMPLE
if (long_condition1 && long_condition2 && long_condition3 && long_condition4) {
#+END_EXAMPLE

and does not leave the formatting to the user.  It can be very annoying for long
lines.

** Astyle

As for GNU indent, Astyle comes with a default formatting (this is not a good
thing).  Beside it cannot toggle alignment off.

Its options have some yet unseen intricacies:

#+BEGIN_QUOTE
Using the k&r option may cause problems because of the &. This can be resolved
by enclosing the k&r in quotes (e.g. --style="k&r") or by using one of the
alternates --style=kr or --style=k/r.
#+END_QUOTE

The [[http://astyle.sourceforge.net/astyle.html][documentation]] is of debatable quality:

#+BEGIN_QUOTE
Also known as Kernel Normal Form (KNF) style, this is the style used in the
Linux kernel.
#+END_QUOTE

KNF is used for the [[https://en.wikipedia.org/wiki/Kernel_Normal_Form][*BSD kernels]]. (See the external links.)

#+BEGIN_QUOTE
"One True Brace Style" formatting/indenting uses linux brackets and adds
brackets to unbracketed one line conditional statements.
#+END_QUOTE

The Linux kernel style already enforces the use of brackets on one-line
statements.  Note that it is not only restricted to conditional statements.  See
the [[https://www.kernel.org/doc/Documentation/CodingStyle][Linux kernel coding style]].

To put it together, it is maintained with [[http://harmful.cat-v.org/software/svn/][Subversion]].

** Uncrustify

It is much more complete than its competitors and comes with an option for
virtually everything.  It will not let you run it without specifying an option
file.  As such it is totally transparent.  Alignment is customizable.

* Algorithm

One of our points in the rationale is dedicated to the simplicity of the
algorithm.  We will investigate a few implementation challenges to help us
understand why additional features such as alignment can impede simplicity.

** Terms

- Opener: a token that increases the indentation of the following part.
  Typically an opening parenthesis/bracket, or a token signaling the beginning
  of a statement.
- Closer: the counter part of an opener.  Typically a closing
  parenthesis/bracket, or a token signaling the end of a statement.
- Middler: a key word found between an opener and a closer.  It can impact
  indentation under specific circumstance.  Typically keywords such as =then= or
  =else=.
- Continuation: a line is said to be continuing if it is semantically attached
  to the previous line.  For instance

  #+BEGIN_EXAMPLE
  foo = bar +
      baz

  foo = "string on\
      multiple lines"
  #+END_EXAMPLE

** Edge cases

Before going any further, let us review a few generic edge cases.

*** Stacked openers and closers

What if several openers appear on the same line?  Shall we indent once or stack
the indent values?  Indenting once looks fine here:

#+BEGIN_EXAMPLE
if cond1 { if cond2 {
    // code
}}
#+END_EXAMPLE

but not right there:

#+BEGIN_EXAMPLE
if cond1 { if cond2 {
    // code
}
}
#+END_EXAMPLE

Besides it breaks the rule of versioning-friendliness from the rationale:

#+BEGIN_EXAMPLE
if cond1 {
     if cond2 {
        // code
}}
#+END_EXAMPLE

This leads us to the closers: should all closers appearing on the same line
unindent once?  The choice is this

#+BEGIN_EXAMPLE
if cond1 { if cond2 { if cond3 {
            // code
        }}}
#+END_EXAMPLE

versus that:

#+BEGIN_EXAMPLE
if cond1 { if cond2 { if cond3 {
            // code
}}}
#+END_EXAMPLE

It is hard to see after 2 indentation levels which statement the closers are
actually closing.  Lisp hackers will probably not mind.

However, it should be noted that closers should unindent their own line only if
they appear first.  It is important for the indentation to be meaningful in
regard to block nesting.  In the following it is clearer that =foo()= is called
inside the condition on =cond2=:

#+BEGIN_EXAMPLE
if cond1 then
     if cond2
     then foo() end end
#+END_EXAMPLE

than here:

#+BEGIN_EXAMPLE
if cond1 then
     if cond2
then foo() end end
#+END_EXAMPLE

or there in C:

#+BEGIN_EXAMPLE
if (cond1) {
     if (cond2)
{ foo() } }
#+END_EXAMPLE

*** Continuation

Line continuation is one of the trickiest part of indentation.  It is considered
good practice to keep line width within a reasonable range, typically around 80.
Some lines will eventually end up being too long to fit within the desired
width, and will have to get split.

Determining if a line is continuing or not involves some analysis.  A naive
approach would be:

- Check both the last token of previous line and the first token of current
  line.  At least one of them must be a continuation token (boolean logic,
  newline escape, etc.).

- If current line is continuing and previous line is not, increase indentation.

- If current line is not continuing and previous line is, decrease indentation.

Continuing lines should typically be indented one level up.

#+BEGIN_EXAMPLE
foo = bar +
    baz
#+END_EXAMPLE

For statement conditions, both the opener and the continuation will increase the
indentation level:

#+BEGIN_EXAMPLE
if foo &&
        bar
then ...
#+END_EXAMPLE

The double indentation is not really desired.  The only reason for considering
=if= as an opener is for cases like this:

#+BEGIN_EXAMPLE
if
    foo &&
        bar
then ...
#+END_EXAMPLE

We can easily solve this issue by considering =then= as an opener and =if= as a
continuation token.

In Go, the curly braces are obvious openers and closers.  The =if= and =for=
keywords should be continuation tokens.  In C, the parenthesis adds up to the
indentation, so the problem is still not solved:

#+BEGIN_EXAMPLE
if (foo &&
        bar) {
}
#+END_EXAMPLE

Workaround suggestions:

- Leave it as it is.
- Add an exception.
- Feel free to send me a smarter solution!

Another tricky problem:

#+BEGIN_EXAMPLE
a = foo +
    bar(baz,
    barbaz
) +
    foobar
#+END_EXAMPLE

After the function call, the line is not seen as continuing anymore, so
indentation gets decreased by 1 level, while being increased by 1 level because
of the opener (so no change overall).

It gets even worse when nested:

#+BEGIN_EXAMPLE
a = foo +
    bar(baz +
            bazbaz
    ) +
    foobar

b = foo +
    bar(baz(
            bazbaz
        )
    ) +
    foobar
#+END_EXAMPLE

The solution to this problem involves a more complex continuation algorithm.  We
need to use a stack to remember the level of indentation of the nested
continuations.

- Indent line.

- Check if continuing: either the end token on previous line or the start token
  on current line must be a continuation token.

- If current indentation does not match the stack and if continuing, increase
  indent and push the new indentation on the stack.

- If not continuing and current indentation is < stack indentation, pop the
  stack and decrease indent.

The cases not matched by the previous conditions do not change the indentation
level.

** An example algorithm

The following algorithm yields an indentation engine close in practice to the
one found in =gofmt=.  The indentation is computed by looking at current and
previous lines only, thus being efficient.

What if previous line indentation is wrong? It does not matter, as indenting the
whole file at once will make sure every line is indented properly.

/Unmatched/ means /not matched on the same line/. Openers match their respective
closer forward and vice-versa.

/First pass:/

#+BEGIN_EXAMPLE
On current line:
-1 on every closer until an unmatched opener is met, if first token is a closer.
-1 if first token is a middler.

On previous line:
+ current indentation.
+1 on every unmatched opener.
+1 if first token is a middler.
-1 on every unmatched closer if first token is not a closer.
#+END_EXAMPLE

For this last case, it is worth noting that if the first token is a middler,
then the first unmatched closer is compensating the indentation change.

/Second pass:/

#+BEGIN_EXAMPLE
+ continuation indentation.
#+END_EXAMPLE

For the continuation indentation, we use the algorithm from the previous
section.

* References

- [[http://www.emacswiki.org/emacs/TabsAreEvil][EmacsWiki: Tabs are evil]]
- [[https://en.wikipedia.org/wiki/Indent_style][Wikipedia: Indent style]]
- [[https://www.kernel.org/doc/Documentation/CodingStyle][Linux kernel coding style]]
