(require 'ert)

(add-to-list 'load-path ".")
(require 'feed-builder)
(require 'diff)

;; When running interactively, `default-directory' can change within ERT
;; definitions.  We work around this by binding the default directory globally.
(defvar feed-builder-test-dir default-directory)

(defun feed-builder--file-to-string (file)
  "Return the file content as string."
  (with-temp-buffer
    (insert-file-contents file)
    (buffer-string)))

(defun feed-builder--string=-explainer (string-a string-b)
  "Return the diff output of STRING-A and STRING-B"
  (unless (string= string-a string-b)
    (let (result
          (file-a (make-temp-file "feed-builder"))
          (file-b (make-temp-file "feed-builder")))
      (with-temp-file file-a
        (insert string-a))
      (with-temp-file file-b
        (insert string-b))
      ;; TODO: Use `unwind-protect'.
      (setq result
            (with-temp-buffer
              (diff-no-select file-a file-b nil 'no-async (current-buffer))
              (buffer-string)))
      (delete-file file-a)
      (delete-file file-b)
      result)))
(put 'string= 'ert-explainer 'feed-builder--string=-explainer)

(ert-deftest feed-builder-single-page ()
  ""
  ;; TODO: default-directory is not always right set to here when ERT is run
  ;; interactively.  We use a global variable as a workaround but it's not
  ;; pretty not convenient.
  (let ((default-directory feed-builder-test-dir)
        (feed  "single-page.rss"))
    (should
     (string=
      (feed-builder--file-to-string feed)
      (feed-builder-build-string
       feed
       "."
       "https://example.org/"
       "Example feed"
       "Example description"
       0
       "post0-html5-fancy.html")))))
