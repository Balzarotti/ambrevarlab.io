;;; feed-builder.el --- Build webfeeds from HTML files -*- lexical-binding: t -*-

;; Copyright (C) 2018 Pierre Neidhardt <mail@ambrevar.xyz>

;; Author: Pierre Neidhardt <mail@ambrevar.xyz>
;; Maintainer: Pierre Neidhardt <mail@ambrevar.xyz>
;; URL: https://gitlab.com/Ambrevar/ambrevar.gitlab.io
;; Version: 0.0.1
;; Package-Requires: ((emacs "25.1"))
;; Keywords: wp, news, hypermedia, blog, feed, rss

;; This file is not part of GNU Emacs.

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:
;;
;; The entry point is `feed-builder-build': run it over a set of HTML files to
;; produce the corresponding webfeed.
;;
;; This code was inspired by https://github.com/bastibe/org-static-blog by
;; Bastian Bechtold.
;;
;; See https://validator.w3.org/feed/ for an online validation service.

;;; Code:

;; TODO: Support atom.

(require 'rx)
(require 'subr-x)
(require 'dom)

(defvar feed-builder-date-function 'feed-builder-date-default
  "Function to fetch the date from an HTML file.")

(defvar feed-builder-title-function 'feed-builder-title-default
  "Function to fetch the title from an HTML file.")

(defvar feed-builder-body-function 'feed-builder-body-default
  "Function to fecth the HTML body.
This can be customized to choose which part to include.")

(defun feed-builder-date-default (html-file)
  "Return the date from the HTML file.
The date is returned as time value.  See `current-time-string'."
  (if (fboundp 'libxml-parse-html-region)
      (feed-builder-date-libxml html-file)
    (let ((case-fold-search t))
      (with-temp-buffer
        (insert-file-contents html-file)
        (goto-char (point-min))
        ;; TODO: Try with proper Org date.
        (search-forward-regexp (rx line-start "<!-- " (group (* any)) " -->"))
        (if (match-string 1)
            (date-to-time
             (concat (match-string 1) " DummyDateSuffix"))
          0)))))

(defun feed-builder-date-libxml (html-file)
  "Return the date from the HTML file.
The date is returned as time value.  See `current-time-string'.
This uses libxml to parse."
  (with-temp-buffer
    (insert-file-contents html-file)
    (let* ((dom (libxml-parse-html-region (point-min) (point-max)))
           (date (dom-text (car (dom-by-class dom "date")))))
      (date-to-time
       (concat (or (and date
                        (string-match (rx (group (repeat 4 digit) "-" (repeat 2 digit) "-" (repeat 2 digit)))
                                      date)
                        (match-string 1 date))
                   ;; Org publish adds a first comment with the timestamp.
                   (dom-text (car (dom-by-tag dom 'comment))))
               " DummyDateSuffix")))))

(defun feed-builder-title-default (html-file)
  "Return the title from the HTML file."
  (if (fboundp 'libxml-parse-html-region)
      (feed-builder-title-libxml html-file)
    (let ((case-fold-search t))
      (with-temp-buffer
        (insert-file-contents html-file)
        (goto-char (point-min))
        (search-forward-regexp (rx line-start "<title>" (group (* (not (any "<")))) "</title>"))
        (or (match-string 1) "")))))

(defun feed-builder-title-libxml (html-file)
  "Return the date from the HTML file.
The date is returned as time value.  See `current-time-string'.
This uses libxml to parse."
  (with-temp-buffer
    (insert-file-contents html-file)
    (let ((dom (libxml-parse-html-region (point-min) (point-max))))
      (dom-text (car (dom-by-tag dom 'title))))))

(defun feed-builder-string-position (string)
  "Return the point of the end of the first occurence STRING in current buffer.
Return nil if none is found."
  (save-excursion
    (goto-char (point-min))
    (if (search-forward string nil 'noerror)
        (point))))

(defun feed-builder-body-default (html-file &optional url exclude-toc)
  "Return the HTML body as a string.
Relative links are made absolute to URL.
If EXCLUDE-TOC is non-nil, the table-of-contents is not included in the body."
  (with-temp-buffer
    (insert-file-contents html-file)
    ;; TODO: Move to helper function.
    ;; Better: See `org-html-link-use-abs-url'.
    (goto-char (point-min))
    (while (re-search-forward (rx "href=\"" (group (* (not (any "\"")))) "\"") nil t)
      ;; Make relative links absolute.
      (let ((match (match-string 1)))
        (unless (string-match "://" match)
          (goto-char (match-beginning 1))
          (insert (concat (replace-regexp-in-string "/*$" "" url) "/"))
          (goto-char (match-end 0)))))
    ;; TODO: Parse HTML properly.  html-mode procedures are not syntax aware.
    (buffer-substring-no-properties
     (progn
       (goto-char (point-min))
       (unless (and exclude-toc
                    (search-forward "</nav>" nil 'noerror))
         (goto-char (point-min))
         (search-forward "</header>"))
       (point))
     (progn
       (goto-char (point-max))
       (search-backward "<div id=\"postamble\"")
       (point)))))

(defun feed-builder-item (html-file project-dir url &optional date &rest categories)
  "Return a feed item corresponding to HTML-FILE as a string.
PROJECT-DIR is the local root of the website which corresponds to URL.
The item date is set to epoch if DATE is nil.
Optional item CATEGORIES can be specified."
  (let ((dest (expand-file-name html-file project-dir))
        (abs-url (concat (replace-regexp-in-string "/*$" "" url) "/" html-file)))
    (concat
     "<item>\n"
     "  <title>" (or (funcall feed-builder-title-function dest) abs-url)
     "</title>\n"
     "  <description><![CDATA["
     (funcall feed-builder-body-function dest url 'exclude-toc)
     "]]></description>\n"
     (when categories
       (mapconcat (lambda (cat) (concat "  <category>" cat "</category>")) "\n"))
     "  <link>" abs-url "</link>\n"
     (format "  <guid isPermaLink=\"false\">%s</guid>\n" abs-url)
     "  <pubDate>"
     (format-time-string "%a, %d %b %Y %H:%M:%S %z" (or date 0))
     "</pubDate>\n"
     "</item>\n")))

;;;###autoload
(defun feed-builder-build-string (webfeed project-dir url
                                  &optional title description build-date
                                  &rest html-files)
  "Build a webfeed buffer out of HTML-FILES.
Return a webfeed buffer in XML format that contains every blog post in a
machine-readable format.

WEBFEED is the path where the feed is intended to be stored,
relative to PROJECT-DIR.

PROJECT-DIR where HTML-FILES are also assumed to reside.
PROJECT-DIR is the local root of the website hosted at URL.  A
feed can have a TITLE and DESCRIPTION: if not, the URL will be
used.

When BUILD-DATE is nil, use `current-time'.  Otherwise it can be
a time expression as in `current-time'.  0 means EPOCH.

HTML parsing details can be customized through the following
variables:

- `feed-builder-date-function'
- `feed-builder-title-function'
- `feed-builder-body-function'."
  (setq title (or title url))
  (let (feed-items)
    (dolist (html-file html-files)
      (message "Building feed for %s..." html-file)
      (let* ((feed-date (or (funcall feed-builder-date-function
                                     (expand-file-name html-file project-dir))
                            0))
             (feed-text (feed-builder-item html-file project-dir url feed-date)))
        (push (cons feed-date feed-text) feed-items)))
    (with-temp-buffer
      (insert "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n"
              "<rss version=\"2.0\" xmlns:atom=\"http://www.w3.org/2005/Atom\">\n"
              "<channel>\n"
              "<title>" title "</title>\n"
              "<description>" (or description title) "</description>\n"
              "<link>" url "</link>\n"
              (format "<atom:link href=\"%s/%s\" rel=\"self\" type=\"application/rss+xml\"/>\n"
                      (string-trim-right url "/") webfeed)
              "<lastBuildDate>" (format-time-string "%a, %d %b %Y %H:%M:%S %z"
                                                    (or build-date (current-time)))
              "</lastBuildDate>\n")
      (dolist (item (sort feed-items (lambda (x y) (time-less-p (car y) (car x)))))
        (insert (cdr item)))
      (insert "</channel>\n"
              "</rss>\n")
      (buffer-string))))

;;;###autoload
(defun feed-builder-build (webfeed project-dir url
                                   &optional title description
                                   &rest html-files)
  "Build a WEBFEED out of HTML-FILES.
The WEBFEED is an XML file that contains every blog post in a
machine-readable format.

The WEBFEED file is stored in PROJECT-DIR where HTML-FILES are
also assumed to reside.

See `feed-builder-build-string'."
  (with-temp-file (expand-file-name webfeed project-dir)
    (insert (apply 'feed-builder-build-string
                   webfeed
                   project-dir
                   url
                   title
                   description
                   nil
                   html-files))))

(provide 'feed-builder)
;;; feed-builder.el ends here
